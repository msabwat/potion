compass = fn
    x, y when x == 0 and y > 0 -> {:north}
    x, y when x < 0 and y == 0 -> {:west}
    x, y when x < 0 and y > 0 -> {:west,:north}
    x, y when x < 0 and y < 0 -> {:west,:south}
    x, y when x > 0 and y == 0 -> {:east}
    x, y when x > 0 and y > 0 -> {:east,:north}
    x, y when x > 0 and y < 0 -> {:east,:south}
    x, y when x == 0 and y < 0 -> {:south}
    _x,_y -> {:nowhere}
end
